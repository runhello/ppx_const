# ppx_const

A PPX syntax extension for the OCaml programming language. Adds a compile-time "if" statement.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/ppx_const).**
